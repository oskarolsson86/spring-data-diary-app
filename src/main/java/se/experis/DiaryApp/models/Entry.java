package se.experis.DiaryApp.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Entry implements Comparator<Entry>{

    //automatically creates a new id
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public Integer id;

    //the title of the entrey
    @Column(nullable = false)
    public String title;

    //take the date for today and then sends it to String strDate
    @Column
    Date date = Calendar.getInstance().getTime();
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    public String strDate = dateFormat.format(date);

    //the text of the entry. changes text from varchar to text so that the user can put any lenght of text
    @Column(columnDefinition = "text" )
    public String text;

    //the imgurl to the entry
    @Column
    public String imgURL;

    //compare strdate (date) and will sort it so that the newest date is first
    @Override
    public int compare(Entry o1, Entry o2) {
        return o2.strDate.compareTo(o1.strDate);
    }
}
