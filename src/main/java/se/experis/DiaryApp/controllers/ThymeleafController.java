package se.experis.DiaryApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se.experis.DiaryApp.models.Entry;
import se.experis.DiaryApp.repositories.EntryRepository;

import java.util.List;


@Controller
public class ThymeleafController {

    @Autowired
    EntryRepository entryRepository;

    //Creates entries-view. List entries gets everything in the entry class, sorts it by date and returns it
    @GetMapping("/entries-view")
    public String entriesView(Model model) {

        List<Entry> entries = entryRepository.findAll();
        entries.sort(new Entry());
        model.addAttribute("entries",entries);

        return "entries";
    }

    //Creates add-entry and refer to addentry.html
    @GetMapping("/add-entry")
    public String addEntry(Model model) {
        return "addentry";
    }

    //Creates edit/view/id and refer to edit.html
    @GetMapping("/edit-view/{id}")
    public String editEntry(@PathVariable int id, Model model) {
        Entry entry = entryRepository.getById(id);
        model.addAttribute("entry", entry);
        return "edit";
    }
}
