package se.experis.DiaryApp.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.DiaryApp.models.Entry;
import se.experis.DiaryApp.repositories.EntryRepository;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
public class EntryController {

    @Autowired
    private EntryRepository entryRepository;

    //will load the specific site entry/id with correct information
    @GetMapping("/entry/{id}")
    public ResponseEntity<Entry> getEntryById(HttpServletRequest request, @PathVariable Integer id){

        Entry entry;
        HttpStatus resp;

        if(entryRepository.existsById(id)) {
            System.out.println("Entry with id: " + id);
            entry= entryRepository.getById(id);
            resp = HttpStatus.OK;
        } else {
            System.out.println("Entry not found");
            entry = null;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(entry, resp);
    }

    //this will create an new entry
    @PostMapping("/entry")
    public ResponseEntity<Entry> addEntry(HttpServletRequest request, @RequestBody Entry entry){

        entry = entryRepository.save(entry);

        System.out.println("New entry with id: " + entry.id);

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(entry, resp);
    }

    //this will update the entries with a specific id. if you put in title and text it cant be empty. imgurl on the other hand cvan be empty
    @PatchMapping("/entry/{id}")
    public ResponseEntity<Entry> updateEntry(HttpServletRequest request, @RequestBody Entry newEntry, @PathVariable Integer id) {

        Entry entry = null;
        HttpStatus resp;

        if(entryRepository.existsById(id)) {
            Optional<Entry> entryRepo = entryRepository.findById(id);
            entry = entryRepo.get();

            if(newEntry.title != null) {
                entry.title = newEntry.title;
            }

            if(newEntry.imgURL != null) {
                entry.imgURL = newEntry.imgURL;
            }

            if(newEntry.text != null) {
                entry.text = newEntry.text;
            }

            entryRepository.save(entry);

            System.out.println("Updated Entry with id: " + entry.id);
            resp = HttpStatus.OK;
        } else {
            System.out.println("Entry not found with id: " + id);
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(entry, resp);
    }

    //this will delete a entry with a specific id.
    @DeleteMapping("/entry/{id}")
    public ResponseEntity<String> deleteEntry(HttpServletRequest request, @PathVariable Integer id) {

        HttpStatus resp;
        String message = "";

        if(entryRepository.existsById(id)) {
            entryRepository.deleteById(id);
            System.out.println("Deleted Entry with id: " + id);
            message = "SUCCESS";
            resp = HttpStatus.OK;
        } else {
            System.out.println("Entry not found with id: " + id);
            message = "FAIL";
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(message, resp);
    }
}
