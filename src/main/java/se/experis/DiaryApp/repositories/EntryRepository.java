package se.experis.DiaryApp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.DiaryApp.models.Entry;

public interface EntryRepository extends JpaRepository<Entry, Integer> {
    Entry getById(int id);
}
